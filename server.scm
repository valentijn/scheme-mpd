(define-module (mpd server)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 optargs)
  #:use-module (ice-9 regex)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-1)
  #:export (<mpd-server>
            <song>
            new-mpd-server
            mpd-connect
            connected?
            disconnect
            mpd-send))

(define-record-type <mpd-server>
  (make-mpd-server address port socket)
  mpd-server?
  (address mpd-server-address set-mpd-server-address!)
  (port    mpd-server-port    set-mpd-server-port!)
  (socket  mpd-server-socket  set-mpd-server-socket!))

;; Check if the server is still connected
(define (connected? server)
  (let ([socket (mpd-server-socket server)])
    (not (or (port-closed? socket)
             (and (char-ready? socket)
                  (eof-object? (peek-char socket)))))))

;; Disconnect from the server and close the socket
(define (disconnect server)
  (when (connected? server)
    (write-line "close" (mpd-server-socket server)))
  (close (mpd-server-socket server)))

;; Create a new mpd server
(define* (new-mpd-server #:optional (address "127.0.0.1") (port 6600))
  (let ([server (make-mpd-server address port (socket PF_INET SOCK_STREAM 0))])
    (mpd-connect server)
    server))

;; Close the current socket, create a new one and connect to the new one
(define (mpd-reconnect! server)
  (close (mpd-server-socket server))
  (set-mpd-server-socket! server (socket PF_INET SOCK_STREAM 0))
  (mpd-connect server))

;; Connect to the MPD server
(define (mpd-connect server)
  (let ([socket (mpd-server-socket server)]
        [address (mpd-server-address server)]
        [port (mpd-server-port server)])

    ;; Connect to the specified server
    (connect socket AF_INET (inet-pton AF_INET address) port)

    ;; Throw an error if we don't receive an answer from the server
    (if (not (string-match "OK MPD *" (read-line socket)))
        (error "Cannot find find the MPD server with address ~a and port ~a" address port))))

(define (mpd-send server command)
  ;; Reconnect to MPD if the socket is closed by the server
  ;; Sleep for a second to give MPD enough time to reconnect
  (unless (connected? server)
    (mpd-reconnect! server))

  (let ([lines '()]
        [socket (mpd-server-socket server)])

    ;; Allow both a symbol and a string for convenience's sake
    (if (symbol? command)
        ;; Send the command to the server.
        (display (string-append (symbol->string command) "\r\n") socket)
        (display (string-append command "\r\n") socket))

    ;; Check if there is something to read in the socket
    (while #t
      (let ([line (read-line socket)])
        ;; Return all the lines whenever you receive the end of the message signal
        (cond [(string=  line "OK") (break lines)]
              [(string-match "ACK *" line) (break line)]
              [else (set! lines (append lines (list line)))])))))
