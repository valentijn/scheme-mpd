(define-module (mpd client)
  #:use-module (mpd server)
  #:use-module (ice-9 optargs)
  #:use-module (ice-9 receive)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9))


(define (make-playlist-item pos type file)
  (list (cons "pos" pos)
        (cons "type" type)
        (cons "file" file)))

(define (make-song file last-modified artist album title time duration date pos id)
  (list (cons "file" file)
        (cons "last-modified" last-modified)
        (cons "artist" artist)
        (cons "album" album)
        (cons "title" title)
        (cons "duration" (string->number duration))
        (cons "date" date)
        (cons "pos" (string->number pos))
        (cons "id" (string->number id))))

(define-public localhost (new-mpd-server))

(define (parse-arg arg)
  (cond [(eqv? arg #t) "1"]
        [(eqv? arg #f) "0"]
        [(string? arg) arg]
        [(number? arg) (number->string arg)]
        [(pair? arg) (string-append (parse-arg (car arg))
                                    ":"
                                    (parse-arg (cdr arg)))]
        [else (symbol->string arg)]))


;; This procedure breaks a list up into smaller subsets of lists based on an index
(define (list-split-index list index)
  (define (take list index aux)
    (if (or (zero? index) (null? list))
        (cons list (reverse aux))
        (take (cdr list) (- index 1) (cons (car list) aux))))

  (define (helper list)
    (if (<= (length list) 0)
        list
        (let ([res (take list index '())])
          (cons (cdr res) (helper (car res))))))

  (helper list))

(define-syntax mpd-define
  (syntax-rules ()
    [(_ name)
     (define-public (name)
       (mpd-send localhost (symbol->string 'name)))]

    [(_ name arg)
     (define-public (name arg)
       (mpd-send localhost
                 (string-concatenate
                  (list
                   (symbol->string 'name)
                   " "
                   (parse-arg arg)))))]

    [(_ name arg ...)
     (define-public (name arg ...)
       (mpd-send localhost
                 (string-concatenate
                  (map
                   (lambda (x)
                     (string-append (parse-arg x)
                                    " "))
                   (list 'name arg ...)))))]))

;; Playback options
(mpd-define consume state)
(mpd-define crossfade seconds)
(mpd-define mixrampdb deciBels)
(mpd-define mixrampdelay seconds)
(mpd-define random state)
(mpd-define repeat state)
(mpd-define setvol vol)
(mpd-define single state)
(mpd-define replay_gain_mode mode)
(mpd-define replay-gain-status)
(mpd-define volume change)

;; Playback commands
(mpd-define pause pause)
(mpd-define next)
(mpd-define play songpos)
(mpd-define playid songid)
(mpd-define previous)
(mpd-define seek songpos time)
(mpd-define seekid songid time)
(mpd-define seekcur time)
(mpd-define stop)
(define-public (start) (pause #f))

;; The Current Playlist
(mpd-define add uri)
(mpd-define addid uri position)
(mpd-define clear)
(mpd-define swap song1 song2)
(mpd-define status)

(define-public (playlist)
  (map (lambda (item)
         (let ([split (string-split item #\:)])
           (make-playlist-item (string->number (car split))
                               (string->symbol (cadr split))
                               (substring (caddr split) 1))))
       (mpd-send localhost (symbol->string 'playlist))))

(define (mpd->song list)
  (map (lambda (item) (let ([split (string-split item #\:)])
                        (cons (string-downcase! (car split))
                              (substring (cadr split) 1)))) list))

(define-public (currentsong)
  (mpd->song (mpd-send localhost "currentsong")))

(define-public (playlistinfo pos)
  (mpd->song (mpd-send localhost (string-append "pos" (parse-arg pos)))))

(define*-public (playlistid #:optional (id #f))
  (if (and (boolean? id) (not id))
      (split-into-parts (mpd->song (mpd-send localhost "playlistid"))
                        (lambda (item) (string=? (car item) "id")))
      (mpd->song (mpd-send localhost (string-append "playlistid " (if (number? id)
                                                                      (number->string id)
                                                                      id))))))

(define*-public (playlistinfo #:optional (songpos #f))
  (if (and (boolean? songpos) (not songpos))
      (split-into-parts (mpd->song (mpd-send localhost "playlistinfo"))
                        (lambda (item) (string=? (car item) "id")))
      (mpd->song (mpd-send localhost (string-append "playlistid " (parse-arg songpos))))))

(define (split-into-parts list pred)
  (define (splitter list aux)
    (if (or (null? list) (pred (car list)))
        (cons (reverse (cons (car list) aux)) (cdr list))
        (splitter (cdr list) (cons (car list) aux))))

  (let loop ([list list])
    (if (not (null? list))
        (let ([res (splitter list '())])
          (cons (car res)
                (loop (cdr res))))
        '())))
