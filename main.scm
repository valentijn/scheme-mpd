(use-modules (ice-9 getopt-long)
             (mpd server)
             ((mpd client) #:prefix mpd:)
             (ncurses curses)
             (ncurses menu)
             (srfi srfi-9))


(define (main args)
  (define stdscr (initscr))

  (cbreak!)
  (noecho!)
  (keypad! stdscr #t)

  (start-color!)
  (init-pair! 1 COLOR_WHITE COLOR_BLACK)
  (attr-on! stdscr (logior A_BOLD (color-pair 1)))


  (let* ([my-items (map (lambda (song)
                          (new-item (assoc-ref song "pos")
                                    (assoc-ref song "title")))
                        (mpd:playlistid))]
         [my-menu (new-menu my-items)])

    ;; Menu options
    (set-menu-format! my-menu (- (lines) 3) 1)
    (set-menu-mark! my-menu "* ")
    (set-menu-back! my-menu (color-pair 1))

    ;; Draw the menu
    (move stdscr (- (lines) 3) 0)
    (hline stdscr (acs-hline) (cols))

    (move stdscr (- (lines) 2) 0)
    (addstr stdscr "Press 'q' to exit")
    (post-menu my-menu)
    (refresh stdscr)

    (call/cc (lambda (saved)
               ;; Handle moving the focus between the items
               (let loop ([c (getch stdscr)])
                 (let* ([run (lambda (expr) expr (loop (getch stdscr)))]
                        [naction (lambda (action) (run (menu-driver my-menu action)))])
                   (case c
                     ;; You need to hard code variables for some reason.
                     ;; 259 -> KEY_UP, 258 -> KEY_DOWN, 343 -> KEY_ENTER
                     ((#\j #\J #\p #\P 259) (naction REQ_UP_ITEM))
                     ((#\k #\K #\n #\N 258)  (naction REQ_DOWN_ITEM))
                     ((#\nl 343) (run (mpd:play (item-name (current-item my-menu)))))
                     ((#\t) (run (mpd:pause #t)))
                     ((#\l #\f) (run (mpd:next)))
                     [(#\h #\b) (run (mpd:previous))]
                     [(#\c) (run (mpd:clear))]
                     [(#\{) (run (mpd:volume -5))]
                     ((#\}) (run (mpd:volume +5)))
                     ((#\]) (run (mpd:volume +10)))
                     [(#\[) (run (mpd:volume -10))]
                     ((#\q #\Q)  (saved))
                     (else (run #t)))))))
    (endwin)))
